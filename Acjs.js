setInterval(() => {
    
    d = new Date();
    hhrs = d.getHours();
    hmin = d.getMinutes();
    hsec = d.getSeconds();

    hrsrot = 30 * hhrs + hmin/2;
    minrot = 6 * hmin;
    secrot = 6 * hsec;

    sec.style.transform = `rotate(${secrot}deg)`;
    min.style.transform = `rotate(${minrot}deg)`;
    hrs.style.transform = `rotate(${hrsrot}deg)`;

},1000);

